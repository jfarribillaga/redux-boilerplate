var React = require('react');
var ReactDOM = require('react-dom');
var Provider = require('react-redux').Provider;

//var HomePage = require('../src/components/router/home/');
//var CounterPage = require('../src/components/router/counter/');
//var FooPage = require('../src/components/router/foo/');

var configureStore = require('./store/configureStore');
var createStore = require('redux').createStore;
var combineReducers = require('redux').combineReducers;
var Router = require('react-router').Router;
var createHistory  = require('history').createHistory;
var syncReduxAndRouter = require('redux-simple-router').syncReduxAndRouter;
var routeReducer = require('redux-simple-router').routeReducer;
var getRoutes = require('./routes/');
//var Counter = require('../src/components/counter');
//import reducers from '<project-path>/reducers';

const reducers = require('./reducers');
//import reducers from '<project-path>/reducers';

const reducer = combineReducers(Object.assign({}, reducers, {
  routing: routeReducer
}));


const history = createHistory();
const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
      <Router history={history}>
             { getRoutes() }
      </Router>
  </Provider>,
  document.getElementById('app')
)
