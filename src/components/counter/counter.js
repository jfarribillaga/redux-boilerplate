require('./counter.scss');

var React = require('react');
var Component = require('react').Component;
var PropTypes = require('react').PropTypes;
const { connect } = require('react-redux');
const {increment,decrement} = require('../../actions/counter');

class Counter extends Component {
  render() {
    const { increment, decrement, counter } = this.props
    return (
        <div className="counter">
          <div className="active counter-component">
            <h1>{counter} likes</h1>
            <button type="button" className="btn" onClick={increment}>Increment</button>
            <button type="button" className="btn left-mg" onClick={decrement}>Decrement</button>
          </div>
        </div>
    )
  }
}

Counter.propTypes = {
  increment: PropTypes.func.isRequired,
  decrement: PropTypes.func.isRequired,
  counter: PropTypes.number.isRequired
}

module.exports = connect(
  state => ({ counter: state.counter }),
  { increment, decrement}
)(Counter);
