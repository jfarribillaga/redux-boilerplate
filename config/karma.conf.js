var baseConf = require('./karma.base').baseConf;

module.exports = function(config) {
  baseConf.logLevel = config.LOG_INFO;
  baseConf.webpack.devtool = 'inline-source-map';
  baseConf.webpack.cacheDirectory = './tmp';
  //keep browser open for debugging
  baseConf.singleRun = false;
  config.set(baseConf);
}
