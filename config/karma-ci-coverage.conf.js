var coverageConf = require('./karma.base').coverageConf;

module.exports = function(config) {
  coverageConf.logLevel = config.LOG_INFO;
  //necessary polyfills for phantomJS lack of support
  coverageConf.files = [
    './node_modules/phantomjs-polyfill/bind-polyfill.js',
    './node_modules/babel-polyfill/browser.js',
    ].concat(coverageConf.files);
  //removing any sourcemap to increase process speed
  coverageConf.webpack.devtool = 'eval';
  coverageConf.browsers = ['PhantomJS'];
  coverageConf.coverageReporter = {
    type: 'cobertura',
    dir: 'test/coverage/'
  };
  config.set(coverageConf);
}
